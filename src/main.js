import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createStore } from 'vuex'

const store = createStore({
    state: {
        goods: [{ name: 'Молоко', quantity: 10,goodtype:'л' },
        { name: 'Комбікорм', quantity: 5,goodtype:'кг' }]
      },
      mutations: {
        addGoods(state, goodsItem) {
          state.goods.push(goodsItem);
        },
        issueGoods(state, goodsItem) {
          const index = state.goods.findIndex(item => item.name === goodsItem.name);
    
          if (index !== -1) {
            
            App.set(state.goods, index, {
              ...state.goods[index],
              quantity: Math.max(0, state.goods[index].quantity - goodsItem.quantity)
            });
          }
        },
        deleteGoods(state, index) {
            if (index >= 0 && index < state.goods.length) {
              state.goods.splice(index, 1);
            }
          },
      },
      actions: {
        addGoods({ commit }, goodsItem) {
          commit('addGoods', goodsItem);
        },
        issueGoods({ commit }, goodsItem) {
          commit('issueGoods', goodsItem);
        },
        deleteGoods({ commit }, index) {
            commit('deleteGoods', index);
          },
       
      }
  })

createApp(App)
.use(router)
.use(store)
.mount('#app')
