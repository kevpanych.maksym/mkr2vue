// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCTa_lDgZZYKAmCE1iNLamVxaoay2bzCLk",
  authDomain: "mkr-app-3b8cd.firebaseapp.com",
  projectId: "mkr-app-3b8cd",
  storageBucket: "mkr-app-3b8cd.appspot.com",
  messagingSenderId: "344204477168",
  appId: "1:344204477168:web:72dfad5d5f03713750b835"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);