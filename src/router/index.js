import { createRouter,createWebHashHistory } from "vue-router";
import GoodsIssue from "../components/GoodsIssue.vue";
import GoodsReceipt from "../components/GoodsReceipt.vue";
import GoodsList from "../components/GoodsList.vue"

const routes=[
    { path: '/', component: GoodsList },
    { path: '/goods-receipt', component: GoodsReceipt },
    { path: '/goods-issue', component: GoodsIssue }
]
const router = createRouter({
    
    history: createWebHashHistory(),
    routes, 
  })
  export default router
